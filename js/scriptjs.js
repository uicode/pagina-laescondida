/*======================
    efecto scroll
=======================*/

smoothScroll.init({
    speed: 1000,
    offset: 75
});

/*======================
    efecto scroll
=======================*/
$(function(){
    $(window).scroll(function(){
        var scrollTop = $(this).scrollTop();
        if(scrollTop >= 50){
            $(".arrow-up").fadeIn();
        }else{
            $(".arrow-up").fadeOut();
        }
    });
});


/*======================
    Cabezera animada
=======================*/

$(window).scroll(function () {

    var nav = $('.encabezado');
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        nav.addClass("fondo-cabezera");
    } else {
        nav.removeClass("fondo-cabezera");
    }

});

